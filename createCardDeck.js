/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck0 = () => {
  const cards =[];
  const suits = ["spades", "diamonds", "clubs", "hearts"];
  const tenCards = ["Jack", "Queen", "King"];

  for (let s of suits){
      for (let i =2; i <= 10; i++){
        const card = {
                      s,
                      val: i,
                      displayVal: i.toString()
                    };
        cards.push(card);
      }

      for ( let tenCard of tenCards){
        cards.push({
                    s,
                    val: 10,
                    displayVal: tenCards
                  } );
      }

      cards.push( {
                  s,
                  val: 11,
                  displayVal: 'Ace'
                } );

      return cards;
  }
 
}

//---------------------------------------------------------//

const getDeck = () => {
  let deck = new Array();
  let suits = ["spades", "diamonds", "clubs", "hearts"];
  const values = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];

  const getVal = (v) => {
    let points;
    switch (v) {
      case 'Ace':
        points = 11;
        break;
      case 'Jack':
      case 'Queen':
      case 'King':
        points = 10;
        break;
      default:
        points = v*1;
    }
    return points;
  };

  for(let i = 0; i < suits.length; i++)
	{
		for(let x = 0; x < values.length; x++)
		{
			let card = {val:getVal(values[x]), displayVal: values[x], suit: suits[i]};
			deck.push(card);
		}
	}
  //console.log(deck);
  return deck;
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);