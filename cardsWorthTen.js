const cards = [
  { val: 2, displayVal: "2", suit: "hearts" },
  { val: 3, displayVal: "3", suit: "hearts" },
  { val: 4, displayVal: "4", suit: "hearts" },
  { val: 5, displayVal: "5", suit: "hearts" },
  { val: 6, displayVal: "6", suit: "hearts" },
  { val: 7, displayVal: "7", suit: "hearts" },
  { val: 8, displayVal: "8", suit: "hearts" },
  { val: 9, displayVal: "9", suit: "hearts" },
  { val: 10, displayVal: "10", suit: "hearts" },
  { val: 10, displayVal: "Jack", suit: "hearts" },
  { val: 10, displayVal: "Queen", suit: "hearts" },
  { val: 10, displayVal: "King", suit: "hearts" },
  { val: 11, displayVal: "Ace", suit: "hearts" }
];

/**
 * Takes an array of cards and returns a string of the card display
 * values where the value is equal to 10
 *
 * @param {array} cards
 * @return {string} displayVal
 */
const cardsWorthTen = cards => {
  let x= cards.filter( (card) =>card.val === 10 );
  let  y= (x.map(i => i.displayVal)).join(',  ');
  return y;
};

const cardsWorthTen1 = cards => {
  return cards
    .filter( card => card.val === 10)
    .map( function(card) {  return card.displayVal;  })
    .join(', ');
}

const cardsWorthTen2 = cards => {
  const x= cards.filter( (card) => card.val === 10 );
  const y = x.map( function(card) {
      return card.displayVal;
  })
  const z = y.join(', ');
  return z;
};

const cardsWorthTen3 = cards => {
  let x= cards.filter( (card) =>card.val === 10 );
  let y= x.map(i => i.displayVal);
  let z = y.join(', ');
  return z;
};

console.log(cardsWorthTen(cards));
// should return/log "10, Jack, Queen, King"


// const header2 = document.createElement("h2");
// const node = document.createTextNode( cardsWorthTen(cards) );
// header2.appendChild(node);
// header2.style.backgroundColor = "lightgreen";
// header2.style.color = "red";
// const element = document.querySelector('body');
// element.appendChild(header2);

const element = document.querySelector('h1');
element.style.fontSize = '30px' ;
element.style.color = 'purple';
element.style.backgroundColor = 'pink';
element.innerHTML =`<b>  ${cardsWorthTen(cards)} </b>`;
