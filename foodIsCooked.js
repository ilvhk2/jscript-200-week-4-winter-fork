/**
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
 */

const foodIsCooked = function(kind, internalTemp, doneness) {
  if (kind === 'chicken'){
    const isFullyCooked =  internalTemp >= 165;
    return isFullyCooked;
  }
  if ( doneness === 'well'){
    return internalTemp >= 155 ? true : false ;
  }
  if ( doneness === 'medium'){
    return internalTemp >= 135;
  }
  if ( doneness === 'rare'){
    return internalTemp >= 125;
  }
}

const foodIsCooked1 = function(kind, internalTemp, doneness) {
  let isCooked = false;
  let done;
  let meat = kind.toLowerCase();
 
  if (arguments.length == 3){ done = doneness.toLowerCase(); }
  
  if (meat === 'chicken' &&  internalTemp >= 165){
         isCooked = true;
  }  else if (meat === 'beef'){
      if (done === 'rare' && internalTemp >= 125){
        isCooked = true;
      } else if (done === 'medium' && internalTemp >= 135){
        isCooked = true;
      } else if (done === 'well' && internalTemp >= 155){
        isCooked = true;
      }
  }
  return isCooked;
}

const foodIsCooked2 = function(kind, internalTemp, doneness) {
    let isCooked = false;
    let meat = kind.toLowerCase();
    // console.log( meat + ' ' + internalTemp);
  if (arguments.length == 2){
    if (meat === 'chicken' &&  internalTemp >= 165){
        isCooked = true;
    } 
  } else if (arguments.length == 3){   
    let done = doneness.toLowerCase();
    // console.log(done);
    if (meat === 'beef'){
      if (done === 'rare' && internalTemp >= 125){
        isCooked = true;
      } else if (done === 'medium' && internalTemp >= 135){
        isCooked = true;
      } else if (done === 'well' && internalTemp >= 155){
        isCooked = true;
      }
    }
  } 
  return isCooked;
}

// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true

console.log ( '-----------------------');
console.log(foodIsCooked1('chicken', 90)); // should be false
console.log(foodIsCooked1('chicken', 190)); // should be true
console.log(foodIsCooked1('beef', 138, 'well')); // should be false
console.log(foodIsCooked1('beef', 138, 'medium')); // should be true
console.log(foodIsCooked1('beef', 138, 'rare')); // should be true

console.log ( '-----------------------');
console.log(foodIsCooked2('chicken', 90)); // should be false
console.log(foodIsCooked2('chicken', 190)); // should be true
console.log(foodIsCooked2('beef', 138, 'well')); // should be false
console.log(foodIsCooked2('beef', 138, 'medium')); // should be true
console.log(foodIsCooked2('beef', 138, 'rare')); // should be true


let node;
const element = document.querySelector('h1');
const header2 = document.createElement("h2");
header2.style.backgroundColor = "lightgreen";
header2.style.color = "red";

node = document.createTextNode( `${foodIsCooked('chicken', 90)} `);
header2.appendChild(node);
element.appendChild(header2);

node = document.createTextNode( `${foodIsCooked('chicken', 190)} `);
header2.appendChild(node);
element.appendChild(header2);

node = document.createTextNode( `${foodIsCooked('beef', 138, 'well')} `);
header2.appendChild(node);
element.appendChild(header2);

node = document.createTextNode( `${foodIsCooked('beef', 138, 'medium')} `);
header2.appendChild(node);
element.appendChild(header2);

node = document.createTextNode( `${foodIsCooked('beef', 138, 'rare')} `);
header2.appendChild(node);
element.appendChild(header2);

