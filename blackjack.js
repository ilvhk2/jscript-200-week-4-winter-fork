const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */

 // 1- CREATE a CardPlayer CLASS
class CardPlayer {
  constructor(name) {
    this.name = name,
    this.hand = [],
    this.drawCard = ()=> {
           //  get a random card 
        const randomCard = blackjackDeck[Math.floor(Math.random() * blackjackDeck.length)];      
        let i = blackjackDeck.indexOf(randomCard);
               // remove the card the the deck --- not required
               //  blackjackDeck.splice(i,1);
        this.hand.push(randomCard);
        console.log( this.hand );
    }
  }
}; //TODO

// 2-  CREATE TWO NEW CardPlayers
const dealer = new CardPlayer('DEALER'); // TODO
const player = new CardPlayer('PLAYER'); // TODO

//------------------------------------------------------------------- testing
// player.drawCard();
// player.drawCard();

// dealer.drawCard();
// dealer.drawCard();

// console.log(` ${player.name} -----------------\n ${player.hand}`);
// console.log(` ${dealer.name} -----------------\n ${dealer.hand}`);
//------------------------------------------------------------------- testing END


// 3-  CREATE calcPoints FUNCTION
/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft - true if there is an Ace in the hand that is being counted as 11 points.  false if the hand has no Aces, or if all Aces are counting as 1 point
 */
const calcPoints = (hand) => {
  let blackJackScore = {};
  blackJackScore.total = 0;
  let aceFlg = 0;
  let aceInd = [];

  for (let i=0; i < hand.length; i++){
    if ( hand[i].val === 11) {
         aceInd.push(i)
        aceFlg += 1;
        if (aceFlg >1 ) {   hand[i].val = 1;   }
        blackJackScore.total += hand[i].val;
       // console.log( `${i}    ${hand[i].val}   ${hand[i].displayVal}    ${hand[i].suit}  total = ${blackJackScore.total}`);    
    } else {
      blackJackScore.total += hand[i].val;
    }
  }
  if ( blackJackScore.total > 21  && aceInd.length>0) {
       hand[aceInd[0]].val = 1;
       blackJackScore.total -= 10;
    //  console.log( `--- ${aceInd[0]}    ${hand[aceInd[0]].val}   ${hand[aceInd[0]].displayVal}    ${hand[aceInd[0]].suit}  total = ${blackJackScore.total}`);   
  };  
  blackJackScore.isSoft = hand.some( (pts) => pts.val === 11 ) ;
  return blackJackScore;
}

//------------------------------------------------------------------- testing
//let playerScore = calcPoints(player.hand).total;

//------------------------------------------------------------------- testing END

// 4- CREATE dealerShouldDraw FUNCTION
/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  console.log('------------------- dealerShouldDraw ---------------');
  //  The dealer must abide by a strict set of rules:
 /*
    If the dealer's hand is 16 points or less, the dealer must draw another card
    If the dealer's hand is exactly 17 points, and the dealer has an Ace valued at 11, the dealer must draw another card
    Otherwise if the dealer's hand is 17 points or more, the dealer will end her turn
*/
  let dealerPoints = calcPoints(dealerHand);
  console.log (" dealerPoints = " + dealerPoints.total);
  if ( dealerPoints.total < 17)  {return true; }
  else if (dealerPoints.total === 17 && dealerPoints.isSoft) {
      dealerPoints.total -= 10;
      dealerPoints.isSoft = false;
      return true;
  } else { return false; }
}

// 5- CREATE determineWinner FUNCTION
/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  if (playerScore === dealerScore ) { 
      return ( `the ${player.name}\'s score is ${playerScore} and the ${dealer.name}\s score is ${dealerScore} ===>   A tie -- No Winner `);
  } else {
      const whoWin = ( playerScore > dealerScore) ?  player.name : dealer.name ;
      return (` the ${player.name}\'s score is ${playerScore} and the ${dealer.name}\s score is ${dealerScore} ===>    ${whoWin} wins`); 
  }
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  let hand = `${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`; 
  console.log ( hand); 
  // console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);

  /* EXTRA CREDIT 
  * display these hands as HTML on the page instead of console.log
  */
  const displayHandHTML = document.getElementById('showHand');
  displayHandHTML.style.color = 'purple';
  displayHandHTML.style.backgroundColor = 'lightgrey';
  displayHandHTML.innerHTML =`<b> ${hand} </b>`;

}

/**
 * Runs Blackjack Game
 */
const startGame1 = function() {
    player.drawCard();
    dealer.drawCard();
    player.drawCard();
    dealer.drawCard();

    let playerScore = calcPoints(player.hand).total;
    showHand(player);    

    while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
      player.drawCard();
      playerScore = calcPoints(player.hand).total;
      showHand(player);
    }
    if (playerScore > 21) {
      return 'You went over 21 - you lose!';
    }
    console.log(`Player stands at ${playerScore}`);

    let dealerScore = calcPoints(dealer.hand).total;
    // console.log ( 'DEALER score ' + dealerScore  + ' ' + dealerShouldDraw(dealer.hand));

    while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
      dealer.drawCard();
      dealerScore = calcPoints(dealer.hand).total;
      showHand(dealer);
    }
    if (dealerScore > 21) {
      return 'Dealer went over 21 - you win!';
    }
    console.log(`Dealer stands at ${dealerScore}`);

    return determineWinner(playerScore, dealerScore);
}

const startGame = function() {
    player.drawCard();
    dealer.drawCard();
    player.drawCard();
    dealer.drawCard();
    let immediatelyWinner ;
    console.log('------------------- PLAYER\'S HAND ---------------');
    let playerScore = calcPoints(player.hand).total;
    showHand(player);
    if ( playerScore === 21)  {
      immediatelyWinner = player.name;
      console.log ( 'immediatelyWinner ' + immediatelyWinner);
    };  

    while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
      player.drawCard();
      playerScore = calcPoints(player.hand).total;
      showHand(player);
    }
    if (playerScore > 21) {
      return 'You went over 21 - you lose!';
    }
    console.log(`Player stands at ${playerScore}`);

  /* EXTRA CREDIT 
  *  	If the player gets exactly 21 after drawing her first 2 cards, the player immediately wins
  * 	If the dealer draws exactly 21 after drawing her first 2 cards, the dealer immediately wins
  */
    console.log('------------------- DEALER\'S HAND ---------------');
    let dealerScore = calcPoints(dealer.hand).total;

    if  (!immediatelyWinner) {
        // console.log ( 'DEALER score ' + dealerScore  + ' ' + dealerShouldDraw(dealer.hand));
        if ( dealerScore === 21)  {
          immediatelyWinner = dealer.name;
          console.log ( 'immediatelyWinner ' + immediatelyWinner);
        };  

        while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
          dealer.drawCard();
          dealerScore = calcPoints(dealer.hand).total;
          showHand(dealer);
        }
        if (dealerScore > 21) {
          return 'Dealer went over 21 - you win!';
        }
        console.log(`Dealer stands at ${dealerScore}`);
    }

    if (!immediatelyWinner) {
        return determineWinner(playerScore, dealerScore);
    }  else {
      // const element = document.getElementById("winner");
      // element.style.fontSize = '30px' ;
      // element.style.color = 'purple';
      // element.style.backgroundColor = 'pink';
      // element.innerHTML =`<b> Immediately Winner: ${immediatelyWinner} </b>`;
      return `Immediately Winner: ${immediatelyWinner} `;
    }
}

 //console.log(startGame());

//  const element = document.getElementById("winner");
//  element.style.fontSize = '30px' ;
//  element.style.color = 'red';
//  element.style.backgroundColor = 'lightyellow';
//  element.innerHTML =`<b> ${startGame()} </b>`;

    const header2 = document.createElement("h2");
    const node = document.createTextNode( startGame() );
    header2.appendChild(node);
    header2.style.backgroundColor = "lightgreen";
    header2.style.color = "red";
       
    const element = document.querySelector('body');
    element.appendChild(header2);